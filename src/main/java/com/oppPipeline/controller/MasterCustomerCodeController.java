package com.oppPipeline.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Exception.OppoPortalServiceException;
import com.oppPipeline.Service.MasterCustomerCodeServiceImpl;

@RestController
@RequestMapping
public class MasterCustomerCodeController
{
  @Autowired
  private MasterCustomerCodeServiceImpl masterCustomerCodeService;
  
  @PostMapping("/addNewMasterCustomerCode")
	 public MasterCustomerCodeInfo addNewMasterCustomerCode (@RequestBody MasterCustomerCodeInfo masterCustomerCodeInfo)
			 throws OppoPortalServiceException {
	    return masterCustomerCodeService.addNewMasterCustomerCode(masterCustomerCodeInfo);
	 }
  
  @GetMapping("/getAllMasterCustomerCode")
  public List<MasterCustomerCodeInfo> getAllMasterCustomerCode ()
			 throws OppoPortalServiceException {
	  return masterCustomerCodeService.getAllMasterCustomerCode();
	 }
 }
