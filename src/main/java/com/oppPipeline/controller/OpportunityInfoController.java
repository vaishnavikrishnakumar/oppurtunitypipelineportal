package com.oppPipeline.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Entity.OpportunityInfo;
import com.oppPipeline.Exception.OppoPortalServiceException;
import com.oppPipeline.Service.OpportunityInfoService;
import com.oppPipeline.excelExporter.OpportuntiyInfoExcelExporter;

@RestController
@RequestMapping
public class OpportunityInfoController {
	
	@Autowired
    private OpportunityInfoService opportunityInfoService;
    
	 @PostMapping("/addNewOpportunity")
	 public OpportunityInfo addNewOpportunity (@RequestBody OpportunityInfo opportunityInfo)
			 throws OppoPortalServiceException {
	  return opportunityInfoService.addNewOpportunity(opportunityInfo);
	 }
  
	 @GetMapping("/getAllOpportunity")
     public List<OpportunityInfo> getAllOpportunity ()
			 throws OppoPortalServiceException {
	  return opportunityInfoService.getAllOpportunity();
	 }
	 
	 
	 @GetMapping("/opportunity/export/excel")
	  public void opportunityToExcel(HttpServletResponse response) throws IOException {
	      response.setContentType("application/octet-stream");
	      //DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
	      //String currentDateTime = dateFormatter.format(new Date());
	       
	      String headerKey = "Content-Disposition";
	      String headerValue = "attachment; filename=MasterCustomerCode.xlsx";
	      response.setHeader(headerKey, headerValue);
	       
	      List<OpportunityInfo> listMCC = opportunityInfoService.getAllOpportunity();
	       
	      //OpportuntiyInfoExcelExporter excelExporter = new OpportuntiyInfoExcelExporter(listMCC);
	       
	      //excelExporter.export(response);    
	  }

}
