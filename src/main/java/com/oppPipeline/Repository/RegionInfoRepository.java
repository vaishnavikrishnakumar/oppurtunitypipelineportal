package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.RegionInfo;

/**
 * 
 * @author Vaishnavi_K02
 *
 */
@Repository
public interface RegionInfoRepository extends JpaRepository<RegionInfo, Integer> 
{

  public RegionInfo findByRegion(String region);
  
}
