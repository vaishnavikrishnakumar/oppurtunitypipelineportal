package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.ContractInfo;

@Repository
public interface ContractInfoRepository extends JpaRepository<ContractInfo, Integer> 
{
	public ContractInfo findByTotalContractValueInfyAndTotalContractValueEquinoxAndContractDurationInMonths (Long totalContractValueInfy,Long totalContractValueEquinox,Integer contractDurationInMonths);
}
