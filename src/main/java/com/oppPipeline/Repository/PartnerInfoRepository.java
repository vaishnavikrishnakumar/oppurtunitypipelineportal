package com.oppPipeline.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.PartnerInfo;

@Repository
public interface PartnerInfoRepository extends JpaRepository<PartnerInfo, Integer> {
	
	public PartnerInfo findByPartnerUnit(String partner);
}
