package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.ServiceOfferInfo;

@Repository
public interface ServiceOfferInfoRepository extends JpaRepository<ServiceOfferInfo, Integer> 
{
	public ServiceOfferInfo findByAnnualSubscriptionAndMonthlyHostingValueAndYearlySupport
	(Integer annualSubscription,String monthlyHostingValue,String yearlySupport);
}
