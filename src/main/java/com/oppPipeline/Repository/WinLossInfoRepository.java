package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.WinLossInfo;

@Repository
public interface WinLossInfoRepository extends JpaRepository<WinLossInfo, Integer> 
{
  
}
