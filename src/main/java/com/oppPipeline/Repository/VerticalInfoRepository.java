package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.VerticalInfo;

/**
 * 
 * @author Vaishnavi_K02
 *
 */
@Repository
public interface VerticalInfoRepository extends JpaRepository<VerticalInfo, Integer> 
{

  public VerticalInfo findByVertical(String vertical);
}
