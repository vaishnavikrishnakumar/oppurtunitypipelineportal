package com.oppPipeline.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.OpportunityInfo;

@Repository
public interface OpportunityInfoRepo extends JpaRepository <OpportunityInfo, Long>{
}
