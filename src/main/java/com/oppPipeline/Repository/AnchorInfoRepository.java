package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.AnchorInfo;

@Repository
public interface AnchorInfoRepository extends JpaRepository<AnchorInfo, Integer> 
{
	public AnchorInfo findByProposalAnchorAndDeliveryAnchor (String proposalAnchor,String deliveryAnchor);
}
