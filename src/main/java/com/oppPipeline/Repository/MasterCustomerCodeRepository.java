package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;

/**
 * 
 * @author Vaishnavi_K02
 *
 */
@Repository
public interface MasterCustomerCodeRepository extends JpaRepository<MasterCustomerCodeInfo, Integer> 
{
  public MasterCustomerCodeInfo findByMasterCustomerCode(String masterCustomerCode);

}
