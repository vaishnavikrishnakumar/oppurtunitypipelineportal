package com.oppPipeline.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.SegmentInfo;

/**
 * 
 * @author Vaishnavi_K02
 *
 */
@Repository
public interface SegmentInfoRepository extends JpaRepository<SegmentInfo, Integer> 
{

  public SegmentInfo findBySegment(String segment);
}
