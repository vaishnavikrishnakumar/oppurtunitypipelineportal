package com.oppPipeline.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oppPipeline.Entity.SalesInfo;

@Repository
public interface SalesInfoRepository extends JpaRepository<SalesInfo, Integer> {
	
	public SalesInfo findByPracticeSalesAndVerticalSales (String practiceSales,String verticalSales);

}
