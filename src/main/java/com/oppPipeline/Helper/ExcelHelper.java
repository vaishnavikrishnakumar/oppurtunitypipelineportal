package com.oppPipeline.Helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Entity.RegionInfo;
import com.oppPipeline.Entity.SegmentInfo;
import com.oppPipeline.Entity.VerticalInfo;

public class ExcelHelper {
  public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  static String[] HEADERs = { "Master Customer Code", "Client Name", "Region", "Segment", "Vertical" };
  static String SHEET = "MasterCustomerCodes";
  public static boolean hasExcelFormat(MultipartFile file) {
    if (!TYPE.equals(file.getContentType())) {
      return false;
    }
    return true;
  }
  public static List<MasterCustomerCodeInfo> excelToMasterCustomerCodeInfos(InputStream is) {
    try {
      Workbook workbook = new XSSFWorkbook(is);
      Sheet sheet = workbook.getSheet(SHEET);
      Iterator<Row> rows = sheet.iterator();
      List<MasterCustomerCodeInfo> masterCustomerCodeInfos = new ArrayList<MasterCustomerCodeInfo>();
      int rowNumber = 0;
      boolean emptyRow = false;
      while (rows.hasNext()) {
        Row currentRow = rows.next();
        // skip header
        if (rowNumber == 0) {
          rowNumber++;
          continue;
        }
        if(emptyRow)
        {
          break;
        }
        Iterator<Cell> cellsInRow = currentRow.iterator();
        MasterCustomerCodeInfo masterCustomerCodeInfo = new MasterCustomerCodeInfo();
        int cellIdx = 0;
        while (cellsInRow.hasNext()) {
          Cell currentCell = cellsInRow.next();
          if(emptyRow)
          {
            break;
          }
          switch (cellIdx) {
          case 0:
            if(StringUtils.isEmpty(currentCell.getStringCellValue()))
            {
              emptyRow = true;
            }
            else
            {
              masterCustomerCodeInfo.setMasterCustomerCode(currentCell.getStringCellValue());
            }
            break;
          case 1:
            masterCustomerCodeInfo.setClientName(currentCell.getStringCellValue());
            break;
          case 2:
            RegionInfo region = new RegionInfo();
            region.setRegion(currentCell.getStringCellValue());         
            masterCustomerCodeInfo.setRegionInfo(region);
            break;
          case 3:
            SegmentInfo segment = new SegmentInfo();
            segment.setSegment(currentCell.getStringCellValue());
            masterCustomerCodeInfo.setSegmentInfo(segment);
            break;
          case 4:
            VerticalInfo vertical = new VerticalInfo();
            vertical.setVertical(currentCell.getStringCellValue());
            masterCustomerCodeInfo.setVerticalInfo(vertical);
            break;
          default:
            break;
          }
          cellIdx++;
        }
        if(!emptyRow)
        {
          masterCustomerCodeInfos.add(masterCustomerCodeInfo);
        }
      }
      workbook.close();
      return masterCustomerCodeInfos;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
    }
  }
}