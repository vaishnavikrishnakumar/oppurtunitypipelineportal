package com.oppPipeline.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Entity.RegionInfo;
import com.oppPipeline.Entity.SegmentInfo;
import com.oppPipeline.Entity.VerticalInfo;
import com.oppPipeline.Repository.MasterCustomerCodeRepository;
import com.oppPipeline.Repository.RegionInfoRepository;
import com.oppPipeline.Repository.SegmentInfoRepository;
import com.oppPipeline.Repository.VerticalInfoRepository;

/**
 * 
 * @author Vaishnavi_K02
 *
 */
@Service
public class MasterCustomerCodeServiceImpl
{
  @Autowired
  private  MasterCustomerCodeRepository masterCustomerCodeRepo;
  
  @Autowired
  private  RegionInfoRepository regionInfoRepo;
  
  @Autowired
  private  SegmentInfoRepository segmentInfoRepo;
  
  @Autowired
  private  VerticalInfoRepository verticalInfoRepo;
   
  /**
   * Method to get all master customer code
   * @return
   */
  public List<MasterCustomerCodeInfo> getMasterCustomerCodes()
  {
    return masterCustomerCodeRepo.findAll();
  }
  
  /**
   * Method to save master customer codes 
   * @param masterCustomerCodes
   */
  public MasterCustomerCodeInfo addNewMasterCustomerCode(MasterCustomerCodeInfo masterCustomerCodes)
  {
     RegionInfo region = masterCustomerCodes.getRegionInfo();
     RegionInfo regionFromDB = regionInfoRepo.findByRegion(region.getRegion());
     Long regionId = regionFromDB !=null ? regionFromDB.getRegionId() : 0;
     if(regionId != 0 )
     {
       masterCustomerCodes.getRegionInfo().setRegionId(regionId);
     }
     else {
        regionInfoRepo.save(region);
        }
     
     SegmentInfo segment = masterCustomerCodes.getSegmentInfo();
     SegmentInfo segmentFromDB = segmentInfoRepo.findBySegment(segment.getSegment());
     
     Long segmentId = segmentFromDB !=null ? segmentFromDB.getSegmentId() : 0;
     if(segmentId !=0 )
     {
       masterCustomerCodes.getSegmentInfo().setSegmentId(segmentId);
     }
     else {
       segmentInfoRepo.save(segment);
     }
     
     VerticalInfo vertical = masterCustomerCodes.getVerticalInfo();
     VerticalInfo verticalFromDB = verticalInfoRepo.findByVertical(vertical.getVertical());
     
     Long verticalId = verticalFromDB !=null ? verticalFromDB.getVerticalId() : 0;
     if(verticalId !=0 )
     {
       masterCustomerCodes.getVerticalInfo().setVerticalId(verticalId);
     }
     else
     {
       verticalInfoRepo.save(vertical); 
     }

     MasterCustomerCodeInfo masterCustInfo =  masterCustomerCodeRepo.findByMasterCustomerCode(masterCustomerCodes.getMasterCustomerCode());
     if(masterCustInfo == null)
     {
       masterCustInfo = masterCustomerCodeRepo.save(masterCustomerCodes);
     }
     return masterCustInfo;
   }

  public List<MasterCustomerCodeInfo> getAllMasterCustomerCode() {
	return masterCustomerCodeRepo.findAll();
  }
}
