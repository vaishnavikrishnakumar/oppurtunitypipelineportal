package com.oppPipeline.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oppPipeline.Entity.AnchorInfo;
import com.oppPipeline.Entity.ContractInfo;
import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Entity.OpportunityInfo;
import com.oppPipeline.Entity.PartnerInfo;
import com.oppPipeline.Entity.SalesInfo;
import com.oppPipeline.Entity.ServiceOfferInfo;
import com.oppPipeline.Repository.AnchorInfoRepository;
import com.oppPipeline.Repository.ContractInfoRepository;
import com.oppPipeline.Repository.MasterCustomerCodeRepository;
import com.oppPipeline.Repository.OpportunityInfoRepo;
import com.oppPipeline.Repository.PartnerInfoRepository;
import com.oppPipeline.Repository.SalesInfoRepository;
import com.oppPipeline.Repository.ServiceOfferInfoRepository;
import com.oppPipeline.Repository.WinLossInfoRepository;

@Service
public class OpportunityInfoService {


	@Autowired
	private OpportunityInfoRepo opportunityInfoRepo;
	@Autowired
	private MasterCustomerCodeRepository masterCustomerCodeRepository;
	
	@Autowired
	private PartnerInfoRepository partnerInfoRepo;
	
	@Autowired
	private SalesInfoRepository salesInfoRepo;
	
	@Autowired
	private AnchorInfoRepository anchorInfoRepo;
	
	@Autowired
	private ServiceOfferInfoRepository serviceOfferInfoRepo;
	
	@Autowired
	private ContractInfoRepository contractInfoRepo;
	
	public OpportunityInfo addNewOpportunity(OpportunityInfo opportunityInfo) {
		
	     MasterCustomerCodeInfo mcc = opportunityInfo.getMasterCustomerCodeInfo();
         MasterCustomerCodeInfo mccFromDB = masterCustomerCodeRepository.findByMasterCustomerCode(mcc.getMasterCustomerCode());
         
	     //PartnerInfo duplicate checking
         
	     PartnerInfo partnerInfo = opportunityInfo.getPartnerInfo();
	     PartnerInfo partnerInfoFromDB = partnerInfoRepo.findByPartnerUnit(partnerInfo.getPartnerUnit());
	     Long partnerId = partnerInfoFromDB !=null ? partnerInfoFromDB.getPartnerUnitId() : 0;
	     if(partnerId != 0 )
	     {
	    	 opportunityInfo.getPartnerInfo().setPartnerUnitId(partnerId);
	     }
	     
	     else {
	    	 
	    	partnerInfoRepo.save(partnerInfo);
	     }
	     
	     //SalesInfo duplicate checking
	     
	     SalesInfo salesInfo = opportunityInfo.getSalesInfo();
	     SalesInfo salesInfoFromDB = salesInfoRepo.findByPracticeSalesAndVerticalSales(salesInfo.getPracticeSales(),
	    		 salesInfo.getVerticalSales());
	     Long salesId = salesInfoFromDB !=null ? salesInfoFromDB.getSalesId() : 0;
	     if(salesId != 0 )
	     {
	    	 opportunityInfo.getSalesInfo().setSalesId(salesId);
	     }
	     else {
	    	salesInfoRepo.save(salesInfo);
	     }
	     
	     //AnchorInfo duplicate checking
	     
	     AnchorInfo anchorInfo = opportunityInfo.getAnchorInfo();
	     AnchorInfo anchorInfoFromDB = anchorInfoRepo.findByProposalAnchorAndDeliveryAnchor(anchorInfo.getProposalAnchor(),
	    		 anchorInfo.getDeliveryAnchor());
	     Long anchorId = anchorInfoFromDB !=null ? anchorInfoFromDB.getAnchorId() : 0;
	     if(anchorId != 0 )
	     {
	    	 opportunityInfo.getAnchorInfo().setAnchorId(anchorId);
	     }
	     else {
	    	anchorInfoRepo.save(anchorInfo);
	     }
	     
	     //ServiceOfferInfo duplicate checking
	     
	     ServiceOfferInfo serviceOfferInfo = opportunityInfo.getServiceOfferInfo();
	     ServiceOfferInfo serviceOfferInfoFromDB = serviceOfferInfoRepo.
	    		 findByAnnualSubscriptionAndMonthlyHostingValueAndYearlySupport
	    		 (serviceOfferInfo.getAnnualSubscription(),serviceOfferInfo.getMonthlyHostingValue(),serviceOfferInfo.getYearlySupport());
	     Long serviceOfferId = serviceOfferInfoFromDB !=null ? serviceOfferInfoFromDB.getServiceOfferId() : 0;
	     if(serviceOfferId != 0 )
	     {
	    	 opportunityInfo.getServiceOfferInfo().setServiceOfferId(serviceOfferId);;
	     }
	     else {
	    	serviceOfferInfoRepo.save(serviceOfferInfo);
	     }
	     
	     //ContractInfo duplicate checking
	     
	     ContractInfo contractInfo = opportunityInfo.getContractInfo();
	     ContractInfo contractInfoFromDB = contractInfoRepo.
	    		 findByTotalContractValueInfyAndTotalContractValueEquinoxAndContractDurationInMonths
	    		 (contractInfo.getTotalContractValueInfy(),contractInfo.getTotalContractValueEquinox(),contractInfo.getContractDurationInMonths());
	     Long contractId = contractInfoFromDB !=null ? contractInfoFromDB.getContractId() : 0;
	     if(contractId != 0 )
	     {
	    	 opportunityInfo.getContractInfo().setContractId(contractId);
	     }
	     else {
	    	contractInfoRepo.save(contractInfo);
	     }

	    opportunityInfo.getMasterCustomerCodeInfo().setMasterCustomerId(mccFromDB.getMasterCustomerId());
	    opportunityInfo.getMasterCustomerCodeInfo().setClientName(mccFromDB.getClientName());
	    opportunityInfo.getMasterCustomerCodeInfo().getRegionInfo().setRegionId(mccFromDB.getRegionInfo().getRegionId());
	    opportunityInfo.getMasterCustomerCodeInfo().getRegionInfo().setRegion(mccFromDB.getRegionInfo().getRegion());
	    opportunityInfo.getMasterCustomerCodeInfo().getSegmentInfo().setSegmentId(mccFromDB.getSegmentInfo().getSegmentId());
	    opportunityInfo.getMasterCustomerCodeInfo().getSegmentInfo().setSegment(mccFromDB.getSegmentInfo().getSegment());
	    opportunityInfo.getMasterCustomerCodeInfo().getVerticalInfo().setVerticalId(mccFromDB.getVerticalInfo().getVerticalId());
	    opportunityInfo.getMasterCustomerCodeInfo().getVerticalInfo().setVertical(mccFromDB.getVerticalInfo().getVertical());
		return opportunityInfoRepo.saveAndFlush(opportunityInfo);
	  }

	  public List<OpportunityInfo> getAllOpportunity() {
		return opportunityInfoRepo.findAll();
		}


}
