package com.oppPipeline.Service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.oppPipeline.Entity.MasterCustomerCodeInfo;
import com.oppPipeline.Helper.ExcelHelper;

@Service
public class ExcelDataService{

  @Autowired
  MasterCustomerCodeServiceImpl mastCustomerCodeService;
  public void save(MultipartFile file) {
    try {
      List<MasterCustomerCodeInfo> masterCustomerCodeInfos = ExcelHelper.excelToMasterCustomerCodeInfos(file.getInputStream());
      for (MasterCustomerCodeInfo masterCustomerCodeInfo : masterCustomerCodeInfos)
      {
        mastCustomerCodeService.addNewMasterCustomerCode(masterCustomerCodeInfo);
      }
      
    } catch (IOException e) {
      throw new RuntimeException("fail to store excel data: " + e.getMessage());
    }
  }
}