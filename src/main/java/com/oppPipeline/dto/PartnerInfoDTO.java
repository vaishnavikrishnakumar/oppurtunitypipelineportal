package com.oppPipeline.dto;

import com.oppPipeline.Entity.PartnerInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartnerInfoDTO {
	
	private Long partnerUnitId;
	
	private String partnerUnit;
    
	public  PartnerInfoDTO preparePartnerInfoDTO(PartnerInfo partnerInfo)
	{
		PartnerInfoDTO partnerInfoDTO = new PartnerInfoDTO();
		partnerInfoDTO.setPartnerUnitId(partnerInfo.getPartnerUnitId());
		partnerInfoDTO.setPartnerUnit(partnerInfo.getPartnerUnit());
		return partnerInfoDTO;
	}
	
	public  PartnerInfo preparePartnerInfo(PartnerInfoDTO partnerInfoDTO)
	   {
		   PartnerInfo partnerInfo = new PartnerInfo();
		   partnerInfo.setPartnerUnitId(partnerInfoDTO.getPartnerUnitId());
		   partnerInfo.setPartnerUnit(partnerInfoDTO.getPartnerUnit());
		   return partnerInfo;
	   }
}
