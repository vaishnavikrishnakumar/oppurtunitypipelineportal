package com.oppPipeline.dto;

import com.oppPipeline.Entity.AnchorInfo;

public class AnchorInfoDTO {
	
	private OpportunityInfoDTO opporutnityInfodto;
	
	private String proposalAnchor;
	
	private String deliveryAnchor;
	
	private String architectAnchor;
    
	public AnchorInfoDTO(OpportunityInfoDTO opporutnityInfodto, String proposalAnchor, String deliveryAnchor,
			String architectAnchor) {
		super();
		this.opporutnityInfodto = opporutnityInfodto;
		this.proposalAnchor = proposalAnchor;
		this.deliveryAnchor = deliveryAnchor;
		this.architectAnchor = architectAnchor;
	}

	public AnchorInfoDTO() {}

	public OpportunityInfoDTO getOpporutnityInfodto() {
		return opporutnityInfodto;
	}

	public void setOpporutnityInfodto(OpportunityInfoDTO opporutnityInfodto) {
		this.opporutnityInfodto = opporutnityInfodto;
	}

	public String getProposalAnchor() {
		return proposalAnchor;
	}

	public void setProposalAnchor(String proposalAnchor) {
		this.proposalAnchor = proposalAnchor;
	}

	public String getDeliveryAnchor() {
		return deliveryAnchor;
	}

	public void setDeliveryAnchor(String deliveryAnchor) {
		this.deliveryAnchor = deliveryAnchor;
	}

	public String getArchitectAnchor() {
		return architectAnchor;
	}

	public void setArchitectAnchor(String architectAnchor) {
		this.architectAnchor = architectAnchor;
	}
	
	public static AnchorInfo prepareAnchorInfoDTO (AnchorInfoDTO anchorInfoDTO) {
		AnchorInfo anchorInfo = new AnchorInfo();
		//anchorInfo.setOpporutnityInfo(OpportunityInfoDTO
		anchorInfo.setArchitectAnchor(anchorInfoDTO.getArchitectAnchor());
		anchorInfo.setDeliveryAnchor(anchorInfoDTO.getDeliveryAnchor());
		anchorInfo.setProposalAnchor(anchorInfoDTO.getProposalAnchor());
		return anchorInfo;
	}
	
	public static AnchorInfoDTO prepareAnchorInfo (AnchorInfo anchorInfo) {
		AnchorInfoDTO anchorInfoDTO = new AnchorInfoDTO();
		//anchorInfoDTO.setOpporutnityInfo(OpportunityInfoDTO
		anchorInfoDTO.setArchitectAnchor(anchorInfo.getArchitectAnchor());
		anchorInfoDTO.setDeliveryAnchor(anchorInfo.getDeliveryAnchor());
		anchorInfoDTO.setProposalAnchor(anchorInfo.getProposalAnchor());
		return anchorInfoDTO;
	}
}
