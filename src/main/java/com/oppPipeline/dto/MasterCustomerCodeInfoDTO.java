package com.oppPipeline.dto;


public class MasterCustomerCodeInfoDTO {
	
	private Long masterCustomerId;
	
	private String masterCustomerCode;
	
	private String clientName;

	private RegionInfoDTO regionInfoDto;
	
	private VerticalInfoDTO verticalInfoDto;
	
	private SegmentInfoDTO segmentInfoDto;
    
	public MasterCustomerCodeInfoDTO(Long masterCustomerId, String masterCustomerCode, String clientName,
			RegionInfoDTO regionInfoDto, VerticalInfoDTO verticalInfoDto, SegmentInfoDTO segmentInfoDto) {
		super();
		this.masterCustomerId = masterCustomerId;
		this.masterCustomerCode = masterCustomerCode;
		this.clientName = clientName;
		this.regionInfoDto = regionInfoDto;
		this.verticalInfoDto = verticalInfoDto;
		this.segmentInfoDto = segmentInfoDto;
	}

	public Long getMasterCustomerId() {
		return masterCustomerId;
	}

	public void setMasterCustomerId(Long masterCustomerId) {
		this.masterCustomerId = masterCustomerId;
	}

	public String getMasterCustomerCode() {
		return masterCustomerCode;
	}

	public void setMasterCustomerCode(String masterCustomerCode) {
		this.masterCustomerCode = masterCustomerCode;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public RegionInfoDTO getRegionInfoDto() {
		return regionInfoDto;
	}

	public void setRegionInfoDto(RegionInfoDTO regionInfoDto) {
		this.regionInfoDto = regionInfoDto;
	}

	public VerticalInfoDTO getVerticalInfoDto() {
		return verticalInfoDto;
	}

	public void setVerticalInfoDto(VerticalInfoDTO verticalInfoDto) {
		this.verticalInfoDto = verticalInfoDto;
	}

	public SegmentInfoDTO getSegmentInfoDto() {
		return segmentInfoDto;
	}

	public void setSegmentInfoDto(SegmentInfoDTO segmentInfoDto) {
		this.segmentInfoDto = segmentInfoDto;
	}
	
}
