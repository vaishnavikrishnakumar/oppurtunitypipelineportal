package com.oppPipeline.dto;

public class SegmentInfoDTO {
	
	private Long segmentId;
	
	private String segment;
    
	public SegmentInfoDTO(Long segmentId, String segment) {
		super();
		this.segmentId = segmentId;
		this.segment = segment;
	}

	public Long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Long segmentId) {
		this.segmentId = segmentId;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}
	

}
