package com.oppPipeline.dto;

public class ServiceOfferInfoDTO {
	
	private OpportunityInfoDTO opportunityInfodto;
	
	private String annualSubscription;
	
	private String licenseValue;
	
	private String coreOffering;
	
	private String monthlyHostingValue;
	
	private String yearlySupport;
    
	public ServiceOfferInfoDTO(OpportunityInfoDTO opportunityInfodto, String annualSubscription, String licenseValue,
			String coreOffering, String monthlyHostingValue, String yearlySupport) {
		super();
		this.opportunityInfodto = opportunityInfodto;
		this.annualSubscription = annualSubscription;
		this.licenseValue = licenseValue;
		this.coreOffering = coreOffering;
		this.monthlyHostingValue = monthlyHostingValue;
		this.yearlySupport = yearlySupport;
	}

	public OpportunityInfoDTO getOpportunityInfodto() {
		return opportunityInfodto;
	}

	public void setOpportunityInfodto(OpportunityInfoDTO opportunityInfodto) {
		this.opportunityInfodto = opportunityInfodto;
	}

	public String getAnnualSubscription() {
		return annualSubscription;
	}

	public void setAnnualSubscription(String annualSubscription) {
		this.annualSubscription = annualSubscription;
	}

	public String getLicenseValue() {
		return licenseValue;
	}

	public void setLicenseValue(String licenseValue) {
		this.licenseValue = licenseValue;
	}

	public String getCoreOffering() {
		return coreOffering;
	}

	public void setCoreOffering(String coreOffering) {
		this.coreOffering = coreOffering;
	}

	public String getMonthlyHostingValue() {
		return monthlyHostingValue;
	}

	public void setMonthlyHostingValue(String monthlyHostingValue) {
		this.monthlyHostingValue = monthlyHostingValue;
	}

	public String getYearlySupport() {
		return yearlySupport;
	}

	public void setYearlySupport(String yearlySupport) {
		this.yearlySupport = yearlySupport;
	}
	

}
