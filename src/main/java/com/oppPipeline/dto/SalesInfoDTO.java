package com.oppPipeline.dto;


public class SalesInfoDTO {
	
	private OpportunityInfoDTO opportunityInfodto;
	
	private String practiceSales;
	
	private String verticalSales;
	
	private String licenseSale;
    
	public SalesInfoDTO(OpportunityInfoDTO opportunityInfodto, String practiceSales, String verticalSales,
			String licenseSale) {
		super();
		this.opportunityInfodto = opportunityInfodto;
		this.practiceSales = practiceSales;
		this.verticalSales = verticalSales;
		this.licenseSale = licenseSale;
	}

	public OpportunityInfoDTO getOpportunityInfodto() {
		return opportunityInfodto;
	}

	public void setOpportunityInfodto(OpportunityInfoDTO opportunityInfodto) {
		this.opportunityInfodto = opportunityInfodto;
	}

	public String getPracticeSales() {
		return practiceSales;
	}

	public void setPracticeSales(String practiceSales) {
		this.practiceSales = practiceSales;
	}

	public String getVerticalSales() {
		return verticalSales;
	}

	public void setVerticalSales(String verticalSales) {
		this.verticalSales = verticalSales;
	}

	public String getLicenseSale() {
		return licenseSale;
	}

	public void setLicenseSale(String licenseSale) {
		this.licenseSale = licenseSale;
	}
	

}
