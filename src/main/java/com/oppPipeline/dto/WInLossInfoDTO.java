package com.oppPipeline.dto;

public class WInLossInfoDTO {
	
	private OpportunityInfoDTO opportunityInfodto;
	
	private String winLossReason;
	
	private String winLossReasonDetails;
    
	public WInLossInfoDTO(OpportunityInfoDTO opportunityInfodto, String winLossReason, String winLossReasonDetails) {
		super();
		this.opportunityInfodto = opportunityInfodto;
		this.winLossReason = winLossReason;
		this.winLossReasonDetails = winLossReasonDetails;
	}

	public OpportunityInfoDTO getOpportunityInfodto() {
		return opportunityInfodto;
	}

	public void setOpportunityInfodto(OpportunityInfoDTO opportunityInfodto) {
		this.opportunityInfodto = opportunityInfodto;
	}

	public String getWinLossReason() {
		return winLossReason;
	}

	public void setWinLossReason(String winLossReason) {
		this.winLossReason = winLossReason;
	}

	public String getWinLossReasonDetails() {
		return winLossReasonDetails;
	}

	public void setWinLossReasonDetails(String winLossReasonDetails) {
		this.winLossReasonDetails = winLossReasonDetails;
	}
	
}
