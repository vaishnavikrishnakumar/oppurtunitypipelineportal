package com.oppPipeline.dto;

import com.oppPipeline.Entity.VerticalInfo;

public class VerticalInfoDTO {
	
	private Long verticalId;
	
	private String vertical;
    
	public VerticalInfoDTO(Long verticalId, String vertical) {
		super();
		this.verticalId = verticalId;
		this.vertical = vertical;
	}

	public VerticalInfoDTO() {}

	public Long getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(Long verticalId) {
		this.verticalId = verticalId;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		this.vertical = vertical;
	}
	
	public  VerticalInfoDTO prepareVerticalInfoDTO(VerticalInfo verticalInfo)
	{
		VerticalInfoDTO verticalInfoDTO = new VerticalInfoDTO();
		verticalInfoDTO.setVerticalId(verticalInfo.getVerticalId());
		verticalInfoDTO.setVertical(verticalInfo.getVertical());
		return verticalInfoDTO;
	}
	
	public  VerticalInfo prepareVerticalInfo(VerticalInfoDTO verticalInfoDTO)
	   {
		   VerticalInfo verticalInfo = new VerticalInfo();
		   verticalInfo.setVerticalId(verticalInfoDTO.getVerticalId());
		   verticalInfo.setVertical(verticalInfoDTO.getVertical());
		   return verticalInfo;
	   }
	

}
