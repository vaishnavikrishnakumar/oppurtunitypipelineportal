package com.oppPipeline.dto;


public class ContractInfoDTO {
	
	private OpportunityInfoDTO opporutnityInfodto;
	
	private Long totalContractValueInfy;
	
	private Long totalContractValueEquinox;
	
	private int contractDurationInMonths;
    
	public ContractInfoDTO(OpportunityInfoDTO opporutnityInfodto, Long totalContractValueInfy,
			Long totalContractValueEquinox, int contractDurationInMonths) {
		super();
		this.opporutnityInfodto = opporutnityInfodto;
		this.totalContractValueInfy = totalContractValueInfy;
		this.totalContractValueEquinox = totalContractValueEquinox;
		this.contractDurationInMonths = contractDurationInMonths;
	}

	public OpportunityInfoDTO getOpporutnityInfodto() {
		return opporutnityInfodto;
	}

	public void setOpporutnityInfodto(OpportunityInfoDTO opporutnityInfodto) {
		this.opporutnityInfodto = opporutnityInfodto;
	}

	public Long getTotalContractValueInfy() {
		return totalContractValueInfy;
	}

	public void setTotalContractValueInfy(Long totalContractValueInfy) {
		this.totalContractValueInfy = totalContractValueInfy;
	}

	public Long getTotalContractValueEquinox() {
		return totalContractValueEquinox;
	}

	public void setTotalContractValueEquinox(Long totalContractValueEquinox) {
		this.totalContractValueEquinox = totalContractValueEquinox;
	}

	public int getContractDurationInMonths() {
		return contractDurationInMonths;
	}

	public void setContractDurationInMonths(int contractDurationInMonths) {
		this.contractDurationInMonths = contractDurationInMonths;
	}
	
}
