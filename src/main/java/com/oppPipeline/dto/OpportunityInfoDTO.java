package com.oppPipeline.dto;

import java.util.Date;

import com.oppPipeline.Entity.OpportunityInfo;
import com.oppPipeline.enums.DealStatus;
import com.oppPipeline.enums.OpportunityStatus;

public class OpportunityInfoDTO {
	
	private Long ciaraId;
	
	private PartnerInfoDTO partnerInfodto;

	private RegionInfoDTO regionInfodto;

	private MasterCustomerCodeInfoDTO masterCustomerCodeInfodto;
	
	private DealStatus dealStatus;

	private OpportunityStatus opportunityStatus;

	private String newAccountOpening;
	
	private Integer probablility;
	
	private String remarks;

	private Date creationDate;

	private Date submissionDate;
	
	private Date closingDate;
	
	private Date startDate;
	
	private Date endDate;
	
	private String competitor;
    
	public OpportunityInfoDTO(Long ciaraId, PartnerInfoDTO partnerInfodto, RegionInfoDTO regionInfodto,
			MasterCustomerCodeInfoDTO masterCustomerCodeInfodto, DealStatus dealStatus,
			OpportunityStatus opportunityStatus, String newAccountOpening, int probablility, String remarks,
			Date creationDate, Date submissionDate, Date closingDate, Date startDate, Date endDate, String competitor) {
		super();
		this.ciaraId = ciaraId;
		this.partnerInfodto = partnerInfodto;
		this.regionInfodto = regionInfodto;
		this.masterCustomerCodeInfodto = masterCustomerCodeInfodto;
		this.dealStatus = dealStatus;
		this.opportunityStatus = opportunityStatus;
		this.newAccountOpening = newAccountOpening;
		this.probablility = probablility;
		this.remarks = remarks;
		this.creationDate = creationDate;
		this.submissionDate = submissionDate;
		this.closingDate = closingDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.competitor = competitor;
	}

	public OpportunityInfoDTO() {}

	public Long getCiaraId() {
		return ciaraId;
	}

	public void setCiaraId(Long ciaraId) {
		this.ciaraId = ciaraId;
	}

	public PartnerInfoDTO getPartnerInfodto() {
		return partnerInfodto;
	}

	public void setPartnerInfodto(PartnerInfoDTO partnerInfodto) {
		this.partnerInfodto = partnerInfodto;
	}

	public RegionInfoDTO getRegionInfodto() {
		return regionInfodto;
	}

	public void setRegionInfodto(RegionInfoDTO regionInfodto) {
		this.regionInfodto = regionInfodto;
	}

	public MasterCustomerCodeInfoDTO getMasterCustomerCodeInfodto() {
		return masterCustomerCodeInfodto;
	}

	public void setMasterCustomerCodeInfodto(MasterCustomerCodeInfoDTO masterCustomerCodeInfodto) {
		this.masterCustomerCodeInfodto = masterCustomerCodeInfodto;
	}

	public DealStatus getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(DealStatus dealStatus) {
		this.dealStatus = dealStatus;
	}

	public OpportunityStatus getOpportunityStatus() {
		return opportunityStatus;
	}

	public void setOpportunityStatus(OpportunityStatus opportunityStatus) {
		this.opportunityStatus = opportunityStatus;
	}

	public String getNewAccountOpening() {
		return newAccountOpening;
	}

	public void setNewAccountOpening(String newAccountOpening) {
		this.newAccountOpening = newAccountOpening;
	}

	public int getProbablility() {
		return probablility;
	}

	public void setProbablility(int probablility) {
		this.probablility = probablility;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public Date getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCompetitor() {
		return competitor;
	}

	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}
	
	
	public  OpportunityInfoDTO prepareOpportunityInfoDTO(OpportunityInfo opportunityInfo)
	{
		OpportunityInfoDTO opportunityInfoDTO = new OpportunityInfoDTO();
		opportunityInfoDTO.setCiaraId(opportunityInfo.getCiaraId());
		//opportunityInfoDTO.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		//opportunityInfoDTO.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		//opportunityInfoDTO.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		opportunityInfoDTO.setDealStatus(opportunityInfo.getDealStatus());
        opportunityInfoDTO.setOpportunityStatus(opportunityInfo.getOpportunityStatus());
    	opportunityInfoDTO.setProbablility(opportunityInfo.getProbablility());
    	opportunityInfoDTO.setRemarks(opportunityInfo.getRemarks());
		opportunityInfoDTO.setCreationDate(opportunityInfo.getCreationDate());
		opportunityInfoDTO.setStartDate(opportunityInfo.getStartDate());
		opportunityInfoDTO.setEndDate(opportunityInfo.getEndDate());
		opportunityInfoDTO.setCompetitor(opportunityInfo.getCompetitor());
	    return opportunityInfoDTO;
	}
	
	public  OpportunityInfo prepareOpportunityInfo(OpportunityInfoDTO opportunityInfoDTO)
	   {
		OpportunityInfo opportunityInfo = new OpportunityInfo();
		opportunityInfo.setCiaraId(opportunityInfoDTO.getCiaraId());
		//opportunityInfo.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		//opportunityInfoDTO.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		//opportunityInfoDTO.setPartnerInfodto(opportunityInfo.getPartnerInfo());
		opportunityInfo.setDealStatus(opportunityInfoDTO.getDealStatus());
        opportunityInfo.setOpportunityStatus(opportunityInfoDTO.getOpportunityStatus());
    	opportunityInfo.setProbablility(opportunityInfoDTO.getProbablility());
    	opportunityInfo.setRemarks(opportunityInfoDTO.getRemarks());
		opportunityInfo.setCreationDate(opportunityInfoDTO.getCreationDate());
		opportunityInfo.setStartDate(opportunityInfoDTO.getStartDate());
		opportunityInfoDTO.setEndDate(opportunityInfoDTO.getEndDate());
		opportunityInfoDTO.setCompetitor(opportunityInfoDTO.getCompetitor());
	    return opportunityInfo;
	   }
	
}
