package com.oppPipeline.dto;

import com.oppPipeline.Entity.RegionInfo;

public class RegionInfoDTO {

	private Long regionId;
	
	private String region;
    
	public RegionInfoDTO(Long regionId, String region) {
		super();
		this.regionId = regionId;
		this.region = region;
	}

	public RegionInfoDTO() {}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	

	public  RegionInfoDTO prepareRegionInfoDTO(RegionInfo regionInfo)
	{
		RegionInfoDTO regionInfoDTO = new RegionInfoDTO();
		regionInfoDTO.setRegionId(regionInfo.getRegionId());
		regionInfoDTO.setRegion(regionInfo.getRegion());
		return regionInfoDTO;
	}
	
	public  RegionInfo prepareregionInfo(RegionInfoDTO regionInfoDTO)
	{
		RegionInfo regionInfo = new RegionInfo();
		regionInfo.setRegionId(regionInfoDTO.getRegionId());
		regionInfo.setRegion(regionInfoDTO.getRegion());
		return regionInfo;
	}
}
