package com.oppPipeline.Exception;

import lombok.NonNull;
import lombok.extern.log4j.Log4j;


/**
 * Exception thrown from OppoPortal Service
 * 
 * @author Infosys
 * 
 */
@Log4j
public class OppoPortalServiceException extends Exception
{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Exception constructor with Throwable param
   * 
   * @param e
   */
  public OppoPortalServiceException(Throwable e)
  {
    super(e);
  }

  /**
   * Exception constructor with message and Throwable param
   * 
   * @param message
   * @param e
   */
  public OppoPortalServiceException(String message, Throwable e)
  {
    super(message, e);
  }

  /**
   * Exception constructor with message param
   * 
   * @param message
   */
  public OppoPortalServiceException(String message)
  {
    super(message);
  }
  
  /**
   * Constructs a OppoPortalServiceException for the given activityContext and exceptionMessage;
   * logs the created exception and activityContext as an ERROR; and returns the exception
   * for further handling (eg throwing).
   * <p>
   * The log message will be in the format:
   * <code>[activityContext] - exceptionMessage with stack trace</code>  .
   * 
   * @param activityContext activityContext for the purposes of logging
   * @param exceptionMessage message used for the construction of the OppoPortalServiceException and in 
   *           logging
   * @return OppoPortalServiceException exception instance that was created and logged
   */
  public static OppoPortalServiceException reportServiceException(
      @NonNull String activityContext,
      @NonNull String exceptionMessage)
  {
    OppoPortalServiceException exception = new OppoPortalServiceException(exceptionMessage);
    
    log.error(activityContext + ":" + exception);
    
    return exception;
  }
  
  /**
   * Constructs a OppoPortalServiceException for the given activityContext, exceptionMessage and cause exception;
   * logs the created exception and activityContext as an ERROR; and returns the exception
   * for further handling (eg throwing).
   * <p>
   * The log message will be in the format:
   * <code>[activityContext] - exceptionMessage with stack trace and cause stack trace</code>  .
   * 
   * @param activityContext activityContext for the purposes of logging
   * @param exceptionMessage message used for the construction of the OppoPortalServiceException and in 
   *           logging
   * @param exceptionCause exception causing the creation of this OppoPortalServiceException
   * @return OppoPortalServiceException exception instance that was created and logged
    */
  public static OppoPortalServiceException reportServiceException(
      @NonNull String activityContext,
      @NonNull String exceptionMessage,
      Throwable exceptionCause)
  {
    OppoPortalServiceException exception = new OppoPortalServiceException(exceptionMessage, exceptionCause);
    
    log.error( activityContext + ":"+ exception);
    
    return exception;
  }


}
