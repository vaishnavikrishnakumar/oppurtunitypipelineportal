package com.oppPipeline.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;

import com.oppPipeline.enums.DealStatus;
import com.oppPipeline.enums.Nao;
import com.oppPipeline.enums.OpportunityStatus;
import com.oppPipeline.enums.OpportunityType;
import com.oppPipeline.enums.PartnerLed;
import com.oppPipeline.enums.PricingModel;
import com.oppPipeline.enums.TopOpportunity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "opportunity_info")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Proxy(lazy=false)
public class OpportunityInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ciara_id")
	private Long ciaraId;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "partner_unit_id")
	private PartnerInfo partnerInfo;
	
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "master_customer_code_id")
	private MasterCustomerCodeInfo masterCustomerCodeInfo;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "service_offer_id")
	private ServiceOfferInfo serviceOfferInfo;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "anchor_id")
	private AnchorInfo anchorInfo;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "contract_id")
	private ContractInfo contractInfo;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "sales_id")
	private SalesInfo salesInfo;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "win_loss_id")
	private WinLossInfo winLossInfo;
	
	@Column(name = "deal_status")
	@Enumerated(EnumType.STRING)
	private DealStatus dealStatus;
	
	@Column(name = "opportunity_status")
	@Enumerated(EnumType.STRING)
	private OpportunityStatus opportunityStatus;
	
	@Column(name = "partner_led_rfp")
	@Enumerated(EnumType.STRING)
	private PartnerLed partnerLedRfp;
	
	@Column(name = "new_account_opening")
	@Enumerated(EnumType.STRING)
	private Nao newAccountOpening;
	
	@Column(name = "probability")
	private int probablility;
		
	@Column(name = "creation_date")
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	
	@Column(name = "submission_date")
	@Temporal(TemporalType.DATE)
	private Date submissionDate;
	
	@Column(name = "closing_date")
	@Temporal(TemporalType.DATE)
	private Date closingDate;
	
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Column(name = "competitor")
	private String competitor;
	
    @Column(name = "opportunity_type")
	@Enumerated(EnumType.STRING)
	private OpportunityType opportunityType;
	
	@Column(name = "opportunity_name")
	private String opportunityName;
	
	@Column(name = "opportunity_desc")
	private String opportunityDescription;
	
	@Column(name = "top_opportunity")
	@Enumerated(EnumType.STRING)
	private TopOpportunity topOpportunity;

	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "pricing_model")
	@Enumerated(EnumType.STRING)
    private PricingModel pricingModel;
}