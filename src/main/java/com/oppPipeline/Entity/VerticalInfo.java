package com.oppPipeline.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "vertical_info")
@Proxy(lazy = false)
public class VerticalInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "vertical_id")
	private Long verticalId;
	
	private String vertical;
    
	public VerticalInfo(Long verticalId, String vertical) {
		super();
		this.verticalId = verticalId;
		this.vertical = vertical;
	}

	public VerticalInfo() {}

	public Long getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(Long verticalId) {
		this.verticalId = verticalId;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		this.vertical = vertical;
	}
}
