package com.oppPipeline.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "win_loss_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WinLossInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "win_loss_id")
	private Long winLossId;
	
	@Column(name = "win_loss_reason")
	private String winLossReason;
	
	@Column(name = "win_loss_reason_details")
	private String winLossReasonDetails;
    
}
