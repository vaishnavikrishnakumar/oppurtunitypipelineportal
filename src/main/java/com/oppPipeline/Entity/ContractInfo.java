package com.oppPipeline.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contract_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContractInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "contract_id")
	private Long contractId;
	
	@Column(name = "total_contract_value_infy")
	private Long totalContractValueInfy;
	
	@Column(name = "total_contract_value_equinox")
	private Long totalContractValueEquinox;
	
	@Column(name = "contract_duration_in_months")
	private int contractDurationInMonths;
	
}
