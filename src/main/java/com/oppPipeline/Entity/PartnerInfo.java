package com.oppPipeline.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "partner_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartnerInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "partner_unit_id")
	private Long partnerUnitId;
	
	@Column(name = "partner_unit")
	private String partnerUnit;
	
}
