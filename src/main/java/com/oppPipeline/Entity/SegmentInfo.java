package com.oppPipeline.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "segment_info")
@Proxy(lazy = false)
public class SegmentInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "segment_id")
	private Long segmentId;
	
	public SegmentInfo(Long segmentId, String segment) {
		super();
		this.segmentId = segmentId;
		this.segment = segment;
	}
	
	public SegmentInfo() { }

	private String segment;
	
	public Long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Long segmentId) {
		this.segmentId = segmentId;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

}
