package com.oppPipeline.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.oppPipeline.enums.LicenseSale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "sales_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesInfo{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "sales_id")
	private Long salesId;
	
	@Column(name = "practice_sales")
	private String practiceSales;
	
	@Column(name = "vertical_sales")
	private String verticalSales;
	
	@Column(name = "license_sale")
	@Enumerated(EnumType.STRING)
	private LicenseSale licenseSale;
    
}
