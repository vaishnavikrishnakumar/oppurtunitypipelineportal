package com.oppPipeline.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "service_offer_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceOfferInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "service_offer_id")
	private Long ServiceOfferId;
	
	@Column(name = "annual_subscription")
	private Integer annualSubscription;
	
	@Column(name = "monthly_hosting_Value")
	private String monthlyHostingValue;
	
	@Column(name = "yearly_support")
	private String yearlySupport;
    
}
