package com.oppPipeline.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "master_customer_code_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Proxy(lazy=false)
public class MasterCustomerCodeInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "master_customer_code_id")
	private Long masterCustomerId;
  
	@Column(name = "master_customer_code")
	private String masterCustomerCode;
	
	@Column(name = "client_name")
	private String clientName;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "region_id")
	private RegionInfo regionInfo;
		
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "vertical_id")
	private VerticalInfo verticalInfo;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "segment_id")
	private SegmentInfo segmentInfo;

}
