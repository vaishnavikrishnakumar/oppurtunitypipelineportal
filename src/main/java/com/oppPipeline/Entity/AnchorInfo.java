package com.oppPipeline.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "anchor_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnchorInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "anchor_id")
	private Long anchorId;
	
	@Column(name = "proposal_anchor")
	private String proposalAnchor;
	
	@Column(name = "delivery_anchor")
	private String deliveryAnchor;
	
	@Column(name = "architect_anchor")
	private String architectAnchor;
	
}
