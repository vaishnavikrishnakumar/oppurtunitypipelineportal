package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OpportunityType{
	EXTENSION("Extension"), 
	PURSUIT("Pursuit"), 
	RFI( "RFI"), 
	PROTECTIVEPITCH("Protective Pitch");
	
  @JsonValue
	private final String type;

    private OpportunityType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    
}