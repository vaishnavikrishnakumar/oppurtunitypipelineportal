package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TopOpportunity{
	
	STRATEGIC("Strategic"), LARGEDEAL("Large Deal");
	
  @JsonValue
	private final String opportunity;
	
	private TopOpportunity(String opportunity) {
		this.opportunity = opportunity;
		
	}
	public String getOpportunity() {
		return opportunity;
	}
}