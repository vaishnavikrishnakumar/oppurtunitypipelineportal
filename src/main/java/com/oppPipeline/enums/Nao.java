package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Nao{
  
	NO("No"),
	YES("Yes");
  
  @JsonValue
  private final String naoValue;

  private Nao(String naoValue) {
      this.naoValue = naoValue;
  }

  public String getaoValue() {
      return naoValue;
  }
}