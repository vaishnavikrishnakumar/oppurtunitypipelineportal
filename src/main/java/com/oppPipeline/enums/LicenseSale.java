package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LicenseSale {
	
	YES("Yes"),
	No("No");
	
  @JsonValue
	private final String licenseSale;

    private LicenseSale(String licenseSale) {
        this.licenseSale = licenseSale;
    }

    public String getLicenseSale() {
        return licenseSale;
    }
}