package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PartnerLed {
	
	YES("Yes"),
	No("No");
	
  @JsonValue
	private final String partnerLed;

    private PartnerLed(String partnerLed) {
        this.partnerLed = partnerLed;
    }

    public String getPartnerLed() {
        return partnerLed;
    }
}