package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DealStatus {
	
	ABANDONED("Abandoned"),
	CLOSED("Closed"),
	DISQUALIFIED("Disqualified"),
	INPROGRESS("In Progress"),
	LOST("Lost"),
	ONHOLD("On Hold"),
	SELECTEDFORORALS("Selected For Orals"),
	SOWSIGNED("SOW Signed"),
	SUBMITTED("Submitted"),
	WON("Won");
	
  @JsonValue
	private final String status;

    private DealStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}