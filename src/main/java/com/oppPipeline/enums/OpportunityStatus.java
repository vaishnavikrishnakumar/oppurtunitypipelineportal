package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OpportunityStatus{
	
	INPROGRESS("In Progress"),SUBMITTED("Submitted"),WON("Won"),LOST("Lost");
	
  @JsonValue
	private final String status;

    private OpportunityStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
	
}