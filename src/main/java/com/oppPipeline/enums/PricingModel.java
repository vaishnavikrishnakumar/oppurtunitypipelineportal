package com.oppPipeline.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PricingModel {
	
	FP("FP"),
	TANDM("T&M"),
	OUTCOMEBASED("Outcome Based"),
	NOTAPPLICABLE("Not Applicable");
	
  @JsonValue
	private final String pricing;

    private PricingModel(String pricing) {
        this.pricing = pricing;
    }

    public String getPricing() {
        return pricing;
    }
}