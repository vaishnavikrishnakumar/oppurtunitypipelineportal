package com.oppPipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpportunityPortalPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpportunityPortalPipelineApplication.class, args);
	}

}
