--liquibase formatted sql

-- changeset partner_info:insertpartner_Info1 context:1.0.0-DML
INSERT INTO partner_info (partner_unit) VALUES ("DCOM");
-- rollback DELETE FROM partner_info WHERE partner_unit_id=1;

-- changeset partner_info:insertpartner_info2 context:1.0.0-DML
INSERT INTO partner_info (partner_unit) VALUES ("ADM");
-- rollback DELETE FROM Partner_Info WHERE partner_unit_id=2;

-- changeset partner_info:insertpartner_info3 context:1.0.0-DML
INSERT INTO partner_info (partner_unit) VALUES ("External Vendor");
-- rollback DELETE FROM partner_info WHERE partner_unit_id=3;


-- changeset region_info:insertregion_info1 context:1.0.0-DML
INSERT INTO region_info (region) VALUES ("Americas");
-- rollback DELETE FROM region_info WHERE region_id=1;

-- changeset region_info:insertregion_info2 context:1.0.0-DML
INSERT INTO region_info (region) VALUES ("ANZ")
-- rollback DELETE FROM region_info WHERE region_id=2;

-- changeset region_info:insertregion_info3 context:1.0.0-DML
INSERT INTO region_info (region) VALUES ("APAC");
-- rollback DELETE FROM Region_Info WHERE region_id=3;

-- changeset region_info:insertregion_info4 context:1.0.0-DML
INSERT INTO region_info (region) VALUES ("Europe");
-- rollback DELETE FROM Region_Info WHERE region_id=4;

-- changeset region_info:insertregion_info5 context:1.0.0-DML
INSERT INTO region_info (region) VALUES ("MEA");
-- rollback DELETE FROM region_info WHERE region_id=5;


-- changeset segment_info:insertsegment_info1 context:1.0.0-DML
INSERT INTO segment_info (segment) VALUES ("CMT");
-- rollback DELETE FROM segment_info WHERE segment_id=1;

-- changeset segment_info:insertsegment_info2 context:1.0.0-DML
INSERT INTO segment_info (segment) VALUES ("CRL");
-- rollback DELETE FROM segment_info WHERE segment_id=2;

-- changeset segment_info:insertsegment_info3 context:1.0.0-DML
INSERT INTO segment_info (segment) VALUES ("FS");
-- rollback DELETE FROM segment_info WHERE segment_id=3;

-- changeset segment_info:insertsegment_info4 context:1.0.0-DML
INSERT INTO segment_info (segment) VALUES ("HIL");
-- rollback DELETE FROM segment_info WHERE segment_id=4;

-- changeset segment_info:insertsegment_info5 context:1.0.0-DML
INSERT INTO segment_info (segment) VALUES ("MFG SURE");
-- rollback DELETE FROM segment_info WHERE segment_id=5;

-- changeset vertical_info:insertvertical_info1 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("ECS");
-- rollback DELETE FROM vertical_info WHERE vertical_id=1;

-- changeset vertical_info:insertvertical_info2 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("FIS");
-- rollback DELETE FROM vertical_info WHERE vertical_id=2;

-- changeset vertical_info:insertvertical_info3 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("HIL");
-- rollback DELETE FROM vertical_info WHERE vertical_id=3;

-- changeset vertical_info:insertvertical_info4 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("MFG");
-- rollback DELETE FROM vertical_info WHERE vertical_id=4;

-- changeset vertical_info:insertvertical_info5 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("HiTech");
-- rollback DELETE FROM vertical_info WHERE vertical_id=5;

-- changeset vertical_info:insertvertical_info6 context:1.0.0-DML
INSERT INTO vertical_info (vertical) VALUES ("RCL");
-- rollback DELETE FROM vertical_info WHERE vertical_id=6;

