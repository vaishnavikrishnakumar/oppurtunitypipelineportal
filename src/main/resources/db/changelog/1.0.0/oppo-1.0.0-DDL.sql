--liquibase formatted sql

-- changeset vertical_info:createvertical_info context:1.0-DDL
-- comment: testing
CREATE TABLE IF NOT EXISTS vertical_info (
vertical_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
vertical Varchar(100)
);

-- rollback DROP TABLE IF EXISTS vertical_info;

-- changeset segment_info:createsegment_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS segment_info (
segment_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
segment Varchar(100)
);

-- rollback DROP TABLE IF EXISTS segment_info;

-- changeset region_info:createregion_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS region_info(
region_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
region Varchar(500)
);

-- rollback DROP TABLE IF EXISTS region_info;

-- changeset partner_info:createpartner_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS partner_info(
partner_unit_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
partner_unit Varchar(500)
);

-- rollback DROP TABLE IF EXISTS partner_info;

-- changeset master_customer_code_info:createmaster_customer_code_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS master_customer_code_info(
master_customer_code_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
master_customer_code Varchar(500) UNIQUE,
client_name Varchar(500),
region_id BIGINT,
vertical_id BIGINT, 
segment_id BIGINT, 
FOREIGN KEY (region_id) REFERENCES region_info(region_id),
FOREIGN KEY (vertical_id) REFERENCES vertical_info(vertical_id),
FOREIGN KEY (segment_id) REFERENCES segment_info(segment_id)
);

-- rollback DROP TABLE IF EXISTS master_customer_code_info;


-- changeset win_loss_info:createwin_loss_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS win_loss_info (
win_loss_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
win_loss_reason Varchar(500),
win_loss_reason_details Varchar(1000)
);

-- rollback DROP TABLE IF EXISTS win_loss_info;


-- changeset service_offer_info:createservice_offer_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS service_offer_info(
service_offer_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
annual_subscription INT,
monthly_hosting_value Varchar(500),
yearly_support Varchar(500)
);
-- rollback DROP TABLE IF EXISTS service_offer_info;

-- changeset sales_info:createsales_info context:1.0-DDL
CREATE TABLE IF NOT EXISTS sales_info(
sales_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
practice_sales Varchar(500),
vertical_sales Varchar(500),
license_sale Varchar(500)
);

-- rollback DROP TABLE IF EXISTS sales_info;


-- changeset contract_info:createcontract_info context:1.0-DDL
CREATE TABLE IF NOT EXISTS contract_info(
contract_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
total_contract_value_infy BIGINT, 
total_contract_Value_equinox BIGINT, 
contract_duration_in_months BIGINT
);
-- rollback DROP TABLE IF EXISTS contract_info;

-- changeset anchor_info:createanchor_info context:1.0-DDL
CREATE TABLE IF NOT EXISTS anchor_info(
anchor_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
proposal_anchor Varchar(500),
delivery_anchor Varchar(500),
architect_anchor Varchar(500)
);

-- rollback DROP TABLE IF EXISTS anchor_info;

-- changeset opportunity_info:createopportunity_info context:1.0-DDL

CREATE TABLE IF NOT EXISTS opportunity_info (
ciara_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
probability INT,
remarks Varchar(500),
creation_date DATE,
end_date DATE,
pricing_model Varchar(500),
submission_date DATE,
closing_date DATE,
start_date DATE,
competitor Varchar(500),
partner_unit_id BIGINT, 
sales_id BIGINT,
contract_id BIGINT,
anchor_id BIGINT,
win_loss_id BIGINT,
service_offer_id BIGINT,
partner_led_rfp Varchar(500),
master_customer_code_id BIGINT,
deal_status_id Varchar(500),
top_opportunity Varchar(500),
opportunity_status Varchar(500),
new_account_opening Varchar(500),
opportunity_type Varchar(500),
opportunity_name Varchar(500),
opportunity_desc Varchar(500),
FOREIGN KEY (master_customer_code_id) REFERENCES master_customer_code_info(master_customer_code_id),
FOREIGN KEY(partner_unit_id) REFERENCES partner_info(partner_unit_id),
FOREIGN KEY(anchor_id) REFERENCES anchor_info(anchor_id),
FOREIGN KEY(sales_id) REFERENCES sales_info(sales_id),
FOREIGN KEY(contract_id) REFERENCES contract_info(contract_id),
FOREIGN KEY(win_loss_id) REFERENCES win_loss_info(win_loss_id),
FOREIGN KEY(service_offer_id) REFERENCES service_offer_info(service_offer_id)
);

-- rollback DROP TABLE IF EXISTS opportunity_info;
